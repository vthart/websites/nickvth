# https://www.nickvth.nl

This is my personal website about automation and open source software.

This site is built using [Hugo](https://gohugo.io/), [Netlify](https://www.netlify.com) and [Gitlab](https://gitlab.com/vthart/websites/nickvth/).

## Build

```
brew install hugo
hugo server -D
```

http://localhost:1313